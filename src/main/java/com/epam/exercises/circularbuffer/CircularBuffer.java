package com.epam.exercises.circularbuffer;

import java.lang.reflect.Array;
import java.util.*;

public class CircularBuffer<T> {
    private int initialCapacity;
    private Object[] data;
    private int size;
    private int head;
    private int tail;
    T el;

    public CircularBuffer(int initialCapacity) {
        this.initialCapacity = initialCapacity;
        data = new Object[initialCapacity];
        size = 0;
        head = tail = 0;
    }

    public void put(T t) {
        if (isFull()) {
            throw new RuntimeException("buffer was full");
        }
        data[head] = t;
        nextHead();
        size++;
    }

    public T get() {
        if (isEmpty()) {
            throw new RuntimeException("buffer was empty");
        }
        T t = (T) data[tail];
        nextTail();
        size--;
        return t;
    }

    public Object[] toObjectArray() {
        Object[] objects = Arrays.copyOf(data, size);
        int pointer = tail;
        for (int i = 0; i < size; i++) {
            objects[i] = data[pointer];
            pointer = getNextPosition(pointer);
        }
        return objects;
    }

    public T[] toArray() {
        if (isEmpty())
            throw new RuntimeException("buffer was empty");
        T[] arr = (T[]) Array.newInstance(asList().get(0).getClass(), size);
        return asList().toArray(arr);
    }

    public List<T> asList() {
        List<T> list = new ArrayList<>();
        Object[] objects = toObjectArray();
        for (Object o:objects) {
            list.add((T)o);
        }
        return list;
    }

    public void addAll(List<? extends T> toAdd) {
        if (toAdd.size() > initialCapacity - size)
            throw new RuntimeException("buffer was full");
        for (T element : toAdd) {
            put(element);
        }
    }

    public void sort(Comparator<? super T> comparator) {
        T[] elements = toArray();
        Arrays.sort(elements, comparator);
        head = tail;
        size = 0;
        addAll(Arrays.asList(elements));
    }

    public boolean isEmpty() {
        return tail == head && size == 0;
    }

    private boolean isFull() {
        return tail == head && size > 0;
    }

    private void nextHead() {
        head = getNextPosition(head);
    }

    private void nextTail() {
        tail = getNextPosition(tail);
    }

    private int getNextPosition(int i) {
        i++;
        if (i >= initialCapacity) {
            i = i - initialCapacity;
        }
        return i;
    }


}
