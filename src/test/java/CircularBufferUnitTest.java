import com.epam.exercises.circularbuffer.CircularBuffer;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;

public class CircularBufferUnitTest {

    //void put
    //      The put method throws RUNTIME exception if the buffer is full. The buffer is full when the head and the tail points to the same index, but the buffer is not empty.
    @Test(expected = RuntimeException.class)
    public void testPutThrowsRuntimeExceptionWhenFull() {
        CircularBuffer<String> circularBuffer = createAndFillStringCircularBuffer(5);
        circularBuffer.put("hi");
    }

    //void get
    //    This method gets the value at the tail of the buffer.
    @Test
    public void testGetValueFromTailOfBuffer() {
        CircularBuffer<String> circularBuffer = createAndFillStringCircularBuffer(4, "1", "2");
        Assert.assertEquals("1", circularBuffer.get());
    }

    //    The get method throws RUNTIME exception if the buffer is empty. The buffer is empty if the head and the tail points to the same index, and the buffer is not full.
    @Test(expected = RuntimeException.class)
    public void testGetThrowsRuntimeExceptionWhenEmpty() {
        CircularBuffer<String> circularBuffer = new CircularBuffer<>(7);
        circularBuffer.get();
    }

    @Test(expected = RuntimeException.class)
    public void testGetThrowsRuntimeExceptionWhenAllRead() {
        CircularBuffer<String> circularBuffer = createAndFillStringCircularBuffer(5, "1", "2");
        for (int i = 0; i < 3; i++) {
            circularBuffer.get();
        }
    }

    //Object[] toObjectArray()
    //This method returns the buffer as an Object array.
    @Test
    public void testToObjectArrayReturnsObjectArray() {
        CircularBuffer<String> circularBuffer = createAndFillStringCircularBuffer(4);
        Assert.assertEquals(circularBuffer.toObjectArray().getClass(), (Object[].class));
    }

    //The size of the array should be equal to the current number of actual elements in the buffer.
    @Test
    public void testToObjectArrayReturnsArrayWithSizeOfElementsOfBuffer() {
        CircularBuffer circularBuffer = createAndFillStringCircularBuffer(5);
        Assert.assertEquals(5, circularBuffer.toObjectArray().length);
    }

    @Test
    public void testToObjectArrayReturnsArrayWithSizeOfActualElementsOfBuffer() {
        CircularBuffer circularBuffer = createAndFillStringCircularBuffer(5);
        circularBuffer.get();
        Assert.assertEquals(4, circularBuffer.toObjectArray().length);
    }

    @Test
    public void testToObjectArrayReturnsArrayWithSizeOfActualElementsOfBufferIfnNotFull() {
        CircularBuffer circularBuffer = createAndFillStringCircularBuffer(5, "1", "2");
        circularBuffer.get();
        Assert.assertEquals(1, circularBuffer.toObjectArray().length);
    }

    //The first element should be the tail.
    @Test
    public void testToObjectArrayReturnsArrayWithTailFirst() {
        CircularBuffer circularBuffer = createAndFillStringCircularBuffer(5, "1", "2");
        Assert.assertEquals("1", circularBuffer.toObjectArray()[0]);
    }

    //T[] toArray()
    //    This method returns the buffer as a type T array.
    @Test
    public void testToArrayReturnsTArray() {
        CircularBuffer<String> circularBuffer = createAndFillStringCircularBuffer(4);
        Assert.assertEquals(circularBuffer.toArray().getClass(), (String[].class));
    }

    //    The size of the array should be equal to the current number of actual elements in the buffer.
    @Test
    public void testToArrayReturnsArrayWithSizeOfElementsOfBuffer() {
        CircularBuffer circularBuffer = createAndFillStringCircularBuffer(5);
        Assert.assertEquals(5, circularBuffer.toArray().length);
    }

    @Test
    public void testToArrayReturnsArrayWithSizeOfActualElementsOfBuffer() {
        CircularBuffer circularBuffer = createAndFillStringCircularBuffer(5);
        circularBuffer.get();
        Assert.assertEquals(4, circularBuffer.toArray().length);
    }

    @Test
    public void testToArrayReturnsArrayWithSizeOfActualElementsOfBufferIfnNotFull() {
        CircularBuffer circularBuffer = createAndFillStringCircularBuffer(5, "1", "2");
        circularBuffer.get();
        Assert.assertEquals(1, circularBuffer.toArray().length);
    }

    //    The first element should be the tail.
    @Test
    public void testToArrayReturnsArrayWithTailFirst() {
        CircularBuffer circularBuffer = createAndFillStringCircularBuffer(5, "1", "2");
        Assert.assertEquals("1", circularBuffer.toArray()[0]);
    }

    //List<T> asList()
    //    This method returns the buffer as a list with type T.
    @Test
    public void testAsListReturnsTList() {
        CircularBuffer<String> circularBuffer = createAndFillStringCircularBuffer(4);
        Assert.assertEquals(circularBuffer.asList().get(0).getClass(), String.class);
    }

    //    The size of the list should be equal to the current number of actual elements in the buffer.
    @Test
    public void testAsListReturnsListWithSizeOfElementsOfBuffer() {
        CircularBuffer circularBuffer = createAndFillStringCircularBuffer(5);
        Assert.assertEquals(5, circularBuffer.asList().size());
    }

    @Test
    public void testAsListReturnsListWithSizeOfActualElementsOfBuffer() {
        CircularBuffer circularBuffer = createAndFillStringCircularBuffer(5);
        circularBuffer.get();
        Assert.assertEquals(4, circularBuffer.asList().size());
    }

    @Test
    public void testAsListReturnsListWithSizeOfActualElementsOfBufferIfnNotFull() {
        CircularBuffer circularBuffer = createAndFillStringCircularBuffer(5, "1", "2");
        circularBuffer.get();
        Assert.assertEquals(1, circularBuffer.asList().size());
    }

    //    The first element should be the tail.
    @Test
    public void testAsListReturnsListWithTailFirst() {
        CircularBuffer circularBuffer = createAndFillStringCircularBuffer(5, "1", "2");
        Assert.assertEquals("1", circularBuffer.asList().get(0));
    }

    //    void addAll(List<? extends T> toAdd)
//This method adds all elements from a given list (which can be type T and T's subclasses) to the buffer.
    @Test
    public void testAddAllAddsAllElementsFromGivenListOfTClass() {
        CircularBuffer<String> circularBuffer = new CircularBuffer<>(3);
        circularBuffer.addAll(Arrays.asList("1", "2"));
        Assert.assertTrue(circularBuffer.asList().containsAll(Arrays.asList("1", "2")));
    }

    @Test
    public void testAddAllAddsAllElementsFromGivenListOfTSubClass() {
        CircularBuffer<Object> circularBuffer = new CircularBuffer<>(3);
        circularBuffer.addAll(Arrays.asList("1", "2"));
        Assert.assertTrue(circularBuffer.asList().containsAll(Arrays.asList("1", "2")));
    }

    // It throws an exception if there is not enough free space in the buffer to add all elements.
    @Test(expected = RuntimeException.class)
    public void testAddAllThrowsExceptionWhenNotEnoughFreeSpace() {
        CircularBuffer<Object> circularBuffer = new CircularBuffer<>(3);
        circularBuffer.addAll(Arrays.asList("1", "2", "3", "4"));
    }

    //void sort(Comparator<? super T> comparator)
//    This method sorts the buffer with the given comparator.
//    As the order of the elements change in the buffer, the positions of the head and the tail should be maintained as well.
    @Test
    public void testSortWithGivenComparator() {
        CircularBuffer<String> circularBuffer = createAndFillStringCircularBuffer(4, "1", "3", "2");
        String[] arr = new String[]{"1", "2", "3"};
        circularBuffer.sort(Comparator.naturalOrder());
        Assert.assertArrayEquals(arr, circularBuffer.toArray());
        Assert.assertEquals("1", circularBuffer.get());
        Assert.assertEquals("2", circularBuffer.get());
        Assert.assertEquals("3", circularBuffer.get());
    }

    //boolean isEmpty()
    //This method returns true if the buffer is empty, false otherwise.
    @Test
    public void testIsEmptyForEmptyBuffer() {
        CircularBuffer<String> circularBuffer = new CircularBuffer<>(5);
        Assert.assertTrue(circularBuffer.isEmpty());
    }

    @Test
    public void testIsEmptyForNonEmptyBuffer() {
        CircularBuffer<String> circularBuffer = createAndFillStringCircularBuffer(5);
        Assert.assertFalse(circularBuffer.isEmpty());
    }

    private CircularBuffer<String> createAndFillStringCircularBuffer(int capacity) {
        CircularBuffer<String> circularBuffer = new CircularBuffer<>(capacity);
        for (int i = 0; i < capacity; i++) {
            circularBuffer.put(RandomStringUtils.randomAlphabetic(5));
        }
        return circularBuffer;
    }

    private CircularBuffer<String> createAndFillStringCircularBuffer(int capacity, String... values) {
        CircularBuffer<String> circularBuffer = new CircularBuffer<>(capacity);
        int counter = 0;
        for (String value : values) {
            circularBuffer.put(value);
        }
        return circularBuffer;
    }

}

